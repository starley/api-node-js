# Changelog
Todas as mudanças desse projeto serão documentadas nesse arquivo.

O formato foi baseado em [Keep a Changelog](https://keepachangelog.com/pt-BR/1.0.0/),
e esse projeto adere a [Semantic Versioning](https://semver.org/lang/pt-BR/spec/v2.0.0.html).

## [1.1.2] - NÃO PUBLICADA

### Adicionado

- Adiciona integração API ONLYPAY.
- Adiciona HELMET para proteção.
- Adiciona Consulta via SQL usando metodo post.
- Adiciona criptografia na consulta SQL
- Adiciona criptografia de senha do usuário

### Modificado

- Atualiza dependências.
- Modifica JWT para receber token via herader.

### Removido

- Nada foi removido nessa versão!

## [1.0.0] - 20/06/2020

### Adicionado

- Adiciona novos controllers

### Modificado

- Corrige aplicação para o CORS

### Removido

- Nada foi removido nessa versão!



