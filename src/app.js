// Configuração sevidor
/**
 * @author Starley Cazorla
 */

'use sctict'

// Firebase App (the core Firebase SDK) is always required and must be listed first
firebase = require('firebase');

var firebaseConfig = require('./config/firebase')
firebase.initializeApp(firebaseConfig);

var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var helmet = require('helmet');

// Documentação do swagger
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

// Index de rotas
const index = require('./routes/index-route') // Index API
const login = require('./routes/login-route') // Login
const forgot = require('./routes/forgot-route') // Esqueceu a senha
const cadastro = require('./routes/cadastro-route') // Cadastro
const cliente = require('./routes/clientes-route') // Clientes
const vendedor = require('./routes/vendedor-route') // Vendedores
const payment = require('./routes/payment-route') // Pagamento
const sqlGeneric = require('./routes/sqlGeneric-route') // Para uso de SQL Generico
const faleConosco = require('./routes/faleConosco-route') // Para uso de SQL Generico
const metas = require('./routes/metas-route') // Metas

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(function (req, res, next) {
  // res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-access-token");
  // res.header("Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS");
  next();
});

app.use(helmet());

// Rotas abertas
app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/', index);
app.use('/open/auth/login', login)
app.use('/open/auth/forgot', forgot)
app.use('/open/cad', cadastro)

// Rotas fechadas ( token )
app.use('/api/cliente', cliente);
app.use('/api/vendedor', vendedor);
app.use('/api/payment', payment);
app.use('/api/sqlGeneric', sqlGeneric);
app.use('/api/faleConosco', faleConosco);
app.use('/api/metas', metas);

module.exports = app;
