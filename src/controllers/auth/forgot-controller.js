// Configuração Controller
/**
 * @author Starley Cazorla
 */
'use sctict'

// const db = firebase.firestore();
const authService = firebase.auth();

exports.post = async (req, res, next) => {

    var body = req.body;

        await authService.sendPasswordResetEmail(body.userLogin).then(() => {
           
            return res.json({ message: 'Recuperação solicitada com sucesso!' });

        }).catch(error => {
            console.log('Erro:', error);
            return res.json({ message: error });
        })

};
