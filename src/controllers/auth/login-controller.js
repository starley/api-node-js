// Configuração Controller
/**
 * @author Starley Cazorla
 */
'use sctict'

const sql = require('mssql');
const serverConfig = require('../../config/serverConfig')

const db = firebase.firestore();
const authService = firebase.auth();

var CryptoJS = require("crypto-js");

require("dotenv-safe").config();
var jwt = require('jsonwebtoken');

exports.post = async (req, res, next) => {

    var body = req.body;
    
    console.log('Login Recebido:', body);

    /**
     * Descriptografando a senha!
     */
    const newPwd = await decrypt(body.userPwd);
    body.userPwd = newPwd

    await db.doc(`usuarios/${body.userLogin}`).get().then(data => {
        // console.log(data.exists);
        if (!data.exists) {
            return res.json({ auth: false, message: 'Usuário invalido!' });
        }
        else {

            /**
             * Trazendo informações do CNPJ que o vendedor esta vinculado
             */
            sql.connect(serverConfig, function (err) {
                if (err) console.log(err);
                // create Request object
                var request = new sql.Request();
                // query to the database and get the records
                request.query(`SELECT DISTINCT A.strCPF, B.strCNPJPar,
                case
                when intGerente is null AND intSupervisor is null then 'GERENTE'
                when intGerente != '' AND intSupervisor is null then 'SUPERVISOR'
                else 'VENDEDOR' end tipoPerfil 
                FROM DIM_HIERARQUIA A
                INNER JOIN DIM_DISTRIBUIDOR B ON A.IDDIST = B.idtbDist
                WHERE strCPF = '${body.userLogin}'`, function (err, recordset) {
                    if (err) console.log(err)
                    // send records as a response
                     console.log(recordset)
                    if (recordset !== undefined || recordset !== null) {
                        executarLogin(data.data(), recordset[0].strCNPJPar, recordset[0].tipoPerfil);
                    } else {
                        executarLogin(data.data(), 'NAO_ENCONTRADO', 'NAO_ENCONTRADO');
                    }
                });
            });
        }

    }).catch(error => {
        console.log('Erro:', error);
        return res.json({ auth: false, message: error });
    });

    async function executarLogin(dadosRetorno, rertornoSql, tipoPerfil) {
        await authService.signInWithEmailAndPassword(dadosRetorno.email, body.userPwd).then(retorno => {
            // console.log('Retorno login:', retorno);
            const id = retorno.uid;
            var token = jwt.sign({ id }, process.env.SECRET, {
                expiresIn: 3000 // expires in 50min
            });
            return res.json({ auth: true, token: token, dadosLogin: dadosRetorno, userLogin: body.userLogin, strCNPJPar: rertornoSql, tipoPerfil: tipoPerfil });

        }).catch(error => {
            console.log('Erro:', error);
            return res.json({ auth: false, message: error });
        })
    }

};

/**
 * Descriptografar dados
 * @param {*} body 
 */
async function decrypt(body) {
    var bytes = CryptoJS.AES.decrypt(body, 'L0r3al*-/St_Api-=Sql!$ConexaoZ3');
    var originalText = bytes.toString(CryptoJS.enc.Utf8);
    // console.log('Sql Original:', originalText)
    return originalText;
}
