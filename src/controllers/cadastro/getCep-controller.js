// Configuração Controller para cadastro de empresas ou atualização
/**
 * @author Starley Cazorla
 */
'use sctict'

const request = require('request');
const apiCnpj = require('../../config/api-cnpj');


//  Buscando dados do CNPJ
exports.get = async (req, res, next) => {

    const cep = await req.query.cep; // Valor passado pela URL
    console.log('Verificando cep =', req.query.cep);
    var options = {
        method: 'GET',
        url: `https://api.postmon.com.br/v1/cep/${cep}`,
        json: true
    };

    try {
        request(options, function (error, response, body) {
            console.log('error:', error);
            console.log('statusCode:', response && response.statusCode);
            console.log('body:', body);

            return res.json({ body });
        });
    } catch (error) {
        return res.status(response.statusCode).json({ message: error });
    }
};
