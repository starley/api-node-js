// Configuração Controller para cadastro de empresas ou atualização
/**
 * @author Starley Cazorla
 */
'use sctict'

const sql = require('mssql');
const serverConfig = require('../../config/serverConfig')

const request = require('request');
const apiCnpj = require('../../config/api-cnpj');

let retornoCnpj;

//  Buscando dados do CNPJ
exports.get = async (req, res, next) => {

    const cnpj = await req.params.cnpj; // Valor passado pela URL
    try {
        sql.connect(serverConfig, function (err) {
            if (err) console.log(err);
            // create Request object
            var request = new sql.Request();
            // query to the database and get the records
            request.query(`select * from STG_CNPJDist WHERE CNPJCD = '${cnpj}'`, function (err, recordset) {
                if (err) console.log(err)
                // send records as a response
                console.log(recordset)
                // res.send(recordset);
                if (recordset[0].razaoSocial === null) {
                    console.log('Atualiza CNPJ!');
                    verificaCnpj();
                } else {
                    return res.json({ message: 'CNPJ Já CADASTRADO', body: recordset });
                }

            });
        });
    } catch (error) {
        return res.status(response.statusCode).json({ message: error });
    }


    // Verifica se o CNPJ existe na base de dados
    function verificaCnpj() {
        console.log('Verificando CNPJ =', cnpj);
        var options = {
            method: 'GET',
            url: `${apiCnpj.UrlBase}/${cnpj}`,
            headers: { authorization: `${apiCnpj.token}` }
        };

        try {
            request(options, function (error, response, body) {
                console.log('error:', error);
                console.log('statusCode:', response && response.statusCode);
                console.log('body:', body);

                retornoCnpj = JSON.parse(body);

                console.log('retorno', retornoCnpj);

                // Verifica se obteve retorno para realizar a pesquisa e inserção no banco de dados
                if (retornoCnpj !== null && response.statusCode === 200) {
                    console.log('Verificando CNPJ!');
                    atualizarCnpj()
                }

                return res.json({ body: retornoCnpj });
            });
        } catch (error) {
            return res.status(response.statusCode).json({ message: error });
        }
    }

    // Atualiza o banco com as novas informacoes
    function atualizarCnpj() {

        try {
            console.log('Atualizando base!');

            sql.connect(serverConfig, function (err) {
                var request = new sql.Request();

                request.query(`UPDATE STG_CNPJDist SET razaoSocial='${retornoCnpj.name}', tipo='${retornoCnpj.type}', status='${retornoCnpj.registration.status}', 
                fantasia='${retornoCnpj.alias}', logradouro='${retornoCnpj.address.street}', numero='${retornoCnpj.address.number}', setor='${retornoCnpj.address.neighborhood}', 
                cep='${retornoCnpj.address.zip}', cidade='${retornoCnpj.address.city}', estado='${retornoCnpj.address.state}' WHERE CNPJCD='${cnpj}'`, function (err, recordset) {
                    if (err) console.log(err)
                    // send records as a response
                    res.send(recordset);
                    if (!err) console.log('CNPJ ATUALIZADO!')

                });
            });
        } catch (error) {
            return res.status(response.statusCode).json({ message: error });
        }


    }

};
