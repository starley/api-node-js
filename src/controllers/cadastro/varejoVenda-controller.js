// Configuração Controller para cadastro de empresas ou atualização
/**
 * @author Starley Cazorla
 */
'use sctict'

const db = firebase.firestore();
const authService = firebase.auth();
let retornoStatus = [];

const sql = require('mssql');
const serverConfig = require('../../config/serverConfig')

//  Buscando dados do CNPJ Ou CPF no banco
exports.get = async (req, res, next) => {
    let cnpjOuCpf = req.query.cnpjOuCpf;
    let tipo = req.query.tipo;
    console.log(req.query)
    // Caso seja pessoa juridica
    if (tipo === 'cnpj') {
        try {
            sql.connect(serverConfig, function (err) {
                if (err) console.log(err);
                var request = new sql.Request();

                request.query(`select TOP 1 * from DIM_DISTRIBUIDOR WHERE strCNPJPAR = '${cnpjOuCpf}'`, function (err, recordset) {
                    if (err) console.log(err)

                    // Verifica se existe dados com cnpjOuCpf
                    if (recordset[0] !== undefined) {
                        return res.json({ message: 'CNPJ Encontrado!', cnpjCadastrado: true, data: recordset[0] });
                    } else {
                        return res.json({ message: 'O CNPJ digitado não foi encontrado na base de dados!', cnpjCadastrado: false, data: null });
                    }

                });
            });
        } catch (error) {
            return res.status(response.statusCode).json({ message: error });
        }
    }

    // Caso seja pessoa fisica
    if (tipo === 'cpf') {

        try {
            sql.connect(serverConfig, function (err) {
                if (err) console.log(err);
                var request = new sql.Request();

                request.query(`SELECT * from dim_DADOSHIE tp WHERE tp.CPF = '${cnpjOuCpf}'`, function (err, recordset) {
                    if (err) console.log(err)

                    if (recordset[0]) {
                        // return res.json(recordset); message: 'CPF já cadastrado!',cpfcadastrado:true,data:'aqui eu quero um objeto com os dados da pessoa'
                        return res.json({ message: 'CPF já cadastrado!', cpfCadastrado: true, data: recordset[0] });
                    } else {
                        return res.json({ message: 'CPF não cadastrado!', cpfCadastrado: false, data: null });
                    }

                });
            });
        } catch (error) {
            return res.status(response.statusCode).json({ message: error });
        }
    }
}

exports.post = async (req, res, next) => {

    console.log('Cadadastro: ', req.body);

    console.log('Criando usuario de acordo com os dados recebidos!', req.body.email, req.body.userPwd);

    await authService.createUserWithEmailAndPassword(req.body.email, req.body.userPwd).then(async dados => {

        console.log('Uid:', dados.user.uid);
        let tipoDadoUsuarioLogin = '';
        if (req.body.strTipoCadastro === 'varejo') {
            tipoDadoUsuarioLogin = req.body.cnpj
        } else {
            tipoDadoUsuarioLogin = req.body.cpf
        }

        await db.doc(`usuarios/${tipoDadoUsuarioLogin}`).set({

            nome: req.body.strNome,
            email: req.body.email,
            perfil: '',
            telefone: req.body.celular,
            uid: dados.user.uid

        }).then(() => {

            sql.connect(serverConfig, function (err) {
                if (err) console.log(err);

                var request = new sql.Request();

                request.query(`UPDATE DIM_DADOSHIE
                SET                  
                Razao='${req.body.strRazaoSocial}',
                Nome='${req.body.strNome}', 
                Email='${req.body.email}', 
                DTNascimento='${req.body.dataNascimento}', 
                Sexo='${req.body.sexo}', 
                DDDCel='0', Celular='${req.body.celular}', 
                DDD='0',
                Telefone='${req.body.celular}', 
                Endereco='${req.body.logradouro}', 
                Numero='${req.body.numero}', 
                Complemento='${req.body.complemento}', 
                UF='${req.body.estado}', 
                Municipio='${req.body.cidade}',
                Bairro='${req.body.bairro}', 
                CEP='${req.body.cep}' 
                WHERE CPF='${req.body.cpf}' AND CNPJPAR='${req.body.cnpj}';`, async function (err, recordset) {
                    if (err) {
                        console.log(err)
                        return res.json({ message: 'Não foi possivel atualizar os dados!', statusCadastro: false });
                    } else {
                        console.log('Dados inseridos na tabela!')

                        return res.json({ message: 'Seu cadastro foi realizado com sucesso!', statusCadastro: true });
                    }
                });
            });

        });

    }).catch(error => {
        console.log('Erro ao criar login:', error)
        return res.json(error);
    })

};
