// Configuração Controller
/**
 * @author Starley Cazorla
 */
'use sctict'

const sql = require('mssql');
const serverConfig = require('../config/serverConfig')

exports.get = (req, res, next) => {
    sql.connect(serverConfig, function (err) {
        if (err) console.log(err);
        // create Request object
        var request = new sql.Request();
        // query to the database and get the records
        request.query(`select * from Clientes`, function (err, recordset) {
            if (err) console.log(err)
            // send records as a response
            console.log(recordset)
            res.send(recordset);

        });
    });
};

exports.post = (req, res, next) => {
    res.status(200).send(req.body);
};

exports.put = (req, res, next) => {
    const id = req.params.id;
    res.status(201).send({
        id: id,
        item: req.body
    });
};

// exports.delete = (req, res, next) => {
//     const id = req.params.id;
//     res.status(201).send({
//         id: id,
//         item: req.body
//     });
// };