// Configuração Controller
/**
 * @author Starley Cazorla
 */
'use sctict'

const nodemailer = require("nodemailer");

// Configuracao do nodemailer
const transporter = nodemailer.createTransport({
  host: "mail.starlley.dev",
  port: 465,
  secure: true, // use TLS
  auth: {
    user: "apinode@starlley.dev",
    pass: "apinode.st.22"
  },
  tls: {
    // do not fail on invalid certs
    rejectUnauthorized: false
  }
});

exports.post = (req, res, next) => {

  console.log("Formulario de contato:", req.body);
  let emailBody = req.body;
  transporter.sendMail(emailBody, function (error, info) {
    if (error) {
      console.log(error);
      return res.json({ message: error });
    } else {
      console.log('Email sent: ' + info.response);
      return res.json({ message: 'E-mail enviado com sucesso!' });
    }
  });

};
