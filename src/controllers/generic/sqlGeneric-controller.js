// Configuração Controller Generico
/**
 * @author Starley Cazorla
 */
'use sctict'

const sql = require('mssql');
const serverConfig = require('../../config/serverConfig')
var CryptoJS = require("crypto-js");

exports.post = async (req, res, next) => {
    console.log('Sql Criptograda', req.body['body'])

   const sqlQuery = await decrypt(req.body['body']);
    // console.log('Faz a pesquisa: ', sqlQuery)
    sql.connect(serverConfig, function (err) {
        
        if (err) console.log(err);
        // create Request object
        var request = new sql.Request();
        // query to the database and get the records
        request.query(`${sqlQuery}`, function (err, recordset) {
            if (err) console.log(err)
            // send records as a response
            console.log(recordset)
            res.send(recordset);

        });
    });
};

exports.get = (req, res, next) => {
    res.status(200).send(req.body);
};

exports.put = (req, res, next) => {
    const id = req.params.id;
    res.status(201).send({
        id: id,
        item: req.body
    });
};

/**
 * Descriptografar dados
 * @param {*} body 
 */
async function decrypt(body) {
    var bytes = CryptoJS.AES.decrypt(body, 'L0r3al*-/St_Api-=Sql!$ConexaoZ3');
    var originalText = bytes.toString(CryptoJS.enc.Utf8);
    // console.log('Sql Original:', originalText)
    return originalText;
}