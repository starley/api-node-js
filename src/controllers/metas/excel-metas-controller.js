
/**
 * @author Walisson Soares
 */
'use strict';

const db = firebase.firestore();
const authService = firebase.auth();
let retornoStatus = [];
global.__basedir = __dirname;

const sql = require('mssql');
const serverConfig = require('../../config/serverConfig')
const readXlsxFile = require('read-excel-file/node');
const formidable = require('formidable');


function gerarTabela() {
    console.log('Carregando tabela na API!');
    const table = new sql.Table('STG_METAHIERARQUIA');
    table.create = false;
    table.columns.add('CNPJPar', sql.NVarChar(14), { nullable: true });
    table.columns.add('CPFVENDEDOR', sql.NVarChar(11), { nullable: true });
    table.columns.add('NOMEVENDEDOR', sql.NVarChar(100), { nullable: true });
    table.columns.add('MATRICULA', sql.NVarChar(20), { nullable: true });
    table.columns.add('META', sql.NVarChar(20), { nullable: true });
    table.columns.add('CARGO', sql.NVarChar(1), { nullable: true });
    table.columns.add('QUARTER', sql.NVarChar(2), { nullable: true });
    table.columns.add('ANO', sql.NVarChar(4), { nullable: true });
    table.columns.add('MESDOQUARTER', sql.NVarChar(2), { nullable: true });

    return table
}

function criarObjeto(rows) {
    console.log('Criando OBJETO!');
    var metaHierarquia = [];
    for (let index = 0; index < rows.length; index++) {

        metaHierarquia.push({
            CNPJPar: rows[index][0],
            CPFVENDEDOR: rows[index][1],
            NOMEVENDEDOR: rows[index][2],
            MATRICULA: rows[index][3],
            META: rows[index][4],
            CARGO: rows[index][5],
            QUARTER: rows[index][6],
            ANO: rows[index][7],
            MESDOQUARTER: rows[index][8]
        });

    }
    return metaHierarquia;
}

function metaJaExiste(cnpj, cpf, matricula, request) {
    return new Promise(function (resolve, reject) {

        var sql = `SELECT
                (CASE
                    WHEN COUNT(*) >= 1 THEN 1
                ELSE 0
                END) JaExiste
            FROM STG_METAHIERARQUIA
            WHERE CNPJPar = '${cnpj}'
            AND CPFVENDEDOR = '${cpf}'
            AND MATRICULA = '${matricula}'`;

        request.query(sql, function (err, rows, fields) {
            if (err) {
                return reject(err);
            }
            return resolve(rows[0].JaExiste);
        });
    });
}

function atualizarMetaVendedor(cnpj, cpf, nomeVendedor, matricula, meta, cargo, quarter, ano, mesdoquarter, request) {
    console.log('JA EXISTE ATUALIZANDO!', nomeVendedor, cargo, quarter, ano, mesdoquarter,);

    var sql = `UPDATE STG_METAHIERARQUIA
    SET NOMEVENDEDOR = '${nomeVendedor}',
        MATRICULA = '${matricula}',
        META = '${meta}',
        CARGO = '${cargo}',
        QUARTER = '${quarter}',
        ANO = '${ano}',
        MESDOQUARTER = '${mesdoquarter}'
    WHERE CPFVENDEDOR = '${cpf}' AND CNPJPar = '${cnpj}' AND  MATRICULA = '${matricula}'`;

    request.query(sql, function (err, result) {
        // console.log('Aguarda', sql);
        if (err) {
            console.log('Erro ao atualizar: ', err, sql);
            throw err;
        }
        if (result) {
            console.log('-->', result);
        }
    });
}

function insereMetaVendedor(cnpj, cpf, nomeVendedor, matricula, meta, cargo, quarter, ano, mesdoquarter, request) {
    console.log('CRIANDO DADOS!', nomeVendedor, cargo, quarter, ano, mesdoquarter,);

    var sql = `BEGIN
    IF NOT EXISTS (SELECT * FROM STG_METAHIERARQUIA
                    WHERE CPFVENDEDOR = '${cpf}')
    BEGIN
            INSERT INTO STG_METAHIERARQUIA
            (CNPJPar, CPFVENDEDOR, NOMEVENDEDOR, MATRICULA, META, CARGO, QUARTER, ANO, MESDOQUARTER)
            VALUES('${cnpj}', '${cpf}', '${nomeVendedor}', '${matricula}', '${meta}', '${cargo}', '${quarter}', '${ano}', '${mesdoquarter}')
        END
    END;`
    request.query(sql, function (err, result) {
        if (err) {
            console.log('Erro inserir: ', err, sql);
            throw err;
        }
        if (result) {
            console.log('-->', result);
        }

    });
}

function atualizarMetas(STG_METAHIERARQUIA, arrMetaHierarquia, request) {
    return new Promise(function (resolve, reject) {
        var tamanhoArray = arrMetaHierarquia.length;
        for (let index = 0; index < tamanhoArray; index++) {

            metaJaExiste
                (
                    arrMetaHierarquia[index].CNPJPar,
                    arrMetaHierarquia[index].CPFVENDEDOR,
                    arrMetaHierarquia[index].MATRICULA,
                    request
                )
                .then((jaExiste) => {
                    if (jaExiste) {
                        // console.log('SE JA EXISTE ATUALIZA!');
                        atualizarMetaVendedor
                            (
                                arrMetaHierarquia[index].CNPJPar,
                                arrMetaHierarquia[index].CPFVENDEDOR,
                                arrMetaHierarquia[index].NOMEVENDEDOR,
                                arrMetaHierarquia[index].MATRICULA,
                                arrMetaHierarquia[index].META,
                                arrMetaHierarquia[index].CARGO,
                                arrMetaHierarquia[index].QUARTER,
                                arrMetaHierarquia[index].ANO,
                                arrMetaHierarquia[index].MESDOQUARTER,
                                request
                            );
                    }
                    else {
                        // console.log('CRIANDO DADOS!');
                        insereMetaVendedor
                            (
                                arrMetaHierarquia[index].CNPJPar,
                                arrMetaHierarquia[index].CPFVENDEDOR,
                                arrMetaHierarquia[index].NOMEVENDEDOR,
                                arrMetaHierarquia[index].MATRICULA,
                                arrMetaHierarquia[index].META,
                                arrMetaHierarquia[index].CARGO,
                                arrMetaHierarquia[index].QUARTER,
                                arrMetaHierarquia[index].ANO,
                                arrMetaHierarquia[index].MESDOQUARTER,
                                request
                            );


                    }

                    if (tamanhoArray == index + 1) {
                        resolve(STG_METAHIERARQUIA.rows);
                    }

                });
        }
    });
}


exports.upload = (req, res) => {

    readXlsxFile(('../uploads/' + req.file.filename)).then((rows) => {
        rows.shift();
        sql.connect(serverConfig, function (err) {
            if (err) console.log(err);

            var request = new sql.Request();
            const STG_METAHIERARQUIA = gerarTabela();
            var arrMetaHierarquia = criarObjeto(rows);
            var novoRegistros = atualizarMetas(STG_METAHIERARQUIA, arrMetaHierarquia, request);

            novoRegistros.then((rows) => {
                STG_METAHIERARQUIA.rows = rows;
                request.bulk(STG_METAHIERARQUIA, (err, result) => {
                    res.send({ message: 'Planilha enviada com sucesso' });
                });
            });

        });
    });

}

exports.get = (t) => {
    console.log('Download');
}