// Configuração Controller
/**
 * @author Starley Cazorla
 */
'use sctict'

const request = require('request');
const apiOnlyPay = require('../../config/api-onlypay');

exports.post = (req, res, next) => {

    console.log('Pegando token OnlyPay',  req.body)
    var options = {
        method: 'POST',
        "rejectUnauthorized": false, 
        url: `${apiOnlyPay.UrlBaseProd}/partner/auth`,
        body: req.body,
        json: true
    };

    console.log('Req', options)

    try {
            
        request(options, function (error, response, body) {
            console.log('error:', error);
            console.log('statusCode:', response && response.statusCode);
            console.log('body:', body);

            globalTokenOnlyPay = body.token;

            return res.status(response.statusCode).json({ body });
        });
    } catch (error) {
        return res.status(response.statusCode).json({ message: error });
    }
};