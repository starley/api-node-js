// Configuração Controller
/**
 * @author Starley Cazorla
 */
'use sctict'

const request = require('request');
const apiOnlyPay = require('../../config/api-onlypay');

exports.post = (req, res, next) => {

    console.log('/partner/campaign-award', req.body)
    var options = {
        method: 'POST',
        "rejectUnauthorized": false,
        url: `${apiOnlyPay.UrlBaseProd}/partner/campaign-award`,
        headers: { authorization: `bearer ${globalTokenOnlyPay}` },
        body: req.body,
        json: true
    };

    // console.log('Req', options)

    try {

        request(options, function (error, response, body) {
            console.log('error:', error);
            console.log('statusCode:', response && response.statusCode);
            console.log('body:', body);

            return res.json({ body });
        });
    } catch (error) {
        return res.json({ message: error });
    }
};

exports.get = (req, res, next) => {

};

exports.put = (req, res, next) => {

};

