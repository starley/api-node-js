// Configuração Rotas
/**
 * @author Starley Cazorla
 */

'use sctict'

const express = require('express');
const router = express.Router();
const getCnpj = require('../controllers/cadastro/getCnpj-controller');
const vendaVarejo = require('../controllers/cadastro/varejoVenda-controller');
const getCep = require('../controllers/cadastro/getCep-controller')
// const verifyJWT = require("../bin/verifyJWT");

router.get('/cnpj/:cnpj', getCnpj.get) // Faz consulta e insere no banco
router.get('/vendaVarejo', vendaVarejo.get) 
router.get('/getCep',  getCep.get);
router.post('/vendaVarejo', vendaVarejo.post)

module.exports = router;