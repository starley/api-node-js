// Configuração Rotas
/**
 * @author Starley Cazorla
 */

'use sctict'

const express = require('express');
const router = express.Router();
const controller = require('../controllers/cliente-controller');
const verifyJWT = require("../bin/verifyJWT");

router.get('/', verifyJWT, controller.get)
router.post('/', verifyJWT, controller.post);
router.put('/:id', verifyJWT, controller.put);
// router.delete('/:id', controller.delete);


module.exports = router;