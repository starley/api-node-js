// Configuração Rotas
/**
 * @author Starley Cazorla
 */

'use sctict'

const express = require('express');
const router = express.Router();
const controller = require('../controllers/auth/forgot-controller');

router.post('/', controller.post)
// router.put('/:id', controller.put);
// router.delete('/:id', controller.delete);


module.exports = router;