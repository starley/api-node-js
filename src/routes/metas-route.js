// Configuração Rotas
/**
 * @author Walisson Soares
 */

'use sctict'

const express = require('express');
const router = express.Router();
const multer = require('multer');

// Modificando tipo de arquivo
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, '../uploads')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now())
  }
})
const upload = multer({ storage: storage });

const metasController = require('../controllers/metas/excel-metas-controller.js');
// const verifyJWT = require("../bin/verifyJWT");

router.post('/excel/upload', upload.single('arquivo'), metasController.upload)
router.get('/excel/download', metasController.get)

module.exports = router;