// Configuração Rotas de Pagamento
/**
 * @author Starley Cazorla
 */

'use sctict'

const express = require('express');
const router = express.Router();
const verifyJWT = require("../bin/verifyJWT");
const auth_payment = require('../controllers/payment/auth-payment-controller'); // Para autenticação
const campain_award = require('../controllers/payment/campain-award-controller'); // Para envio de campanha

router.post('/auth', verifyJWT, auth_payment.post);
router.post('/campain', verifyJWT, campain_award.post);
router.put('/campain/:id', verifyJWT, campain_award.put);
router.get('/campain', verifyJWT, campain_award.get);


module.exports = router;