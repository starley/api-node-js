// Configuração Rotas de SQL Generico
/**
 * @author Starley Cazorla
 */

'use sctict'

const express = require('express');
const router = express.Router();
const controller = require('../controllers/generic/sqlGeneric-controller');
const verifyJWT = require("../bin/verifyJWT");

router.get('/', verifyJWT, controller.get)
router.post('/', verifyJWT, controller.post);
router.put('/:id', verifyJWT, controller.put);
// router.delete('/:id',verifyJWT, controller.delete);


module.exports = router;